
#include <QCoreApplication>

#include "ServiceLocator.h"

#include <memory> //c++ 17


void somefunction() {
    ServiceLocatorWithTemplates<ILogger>().get().log("hi"); //crashes since it's unset
}

int main(int argc, char *argv[]) {

    QCoreApplication a(argc, argv);

    //Application init

    std::unique_ptr<ILogger> spLogger = std::make_unique<ConsoleLogger>();
    ServiceLocatorWithTemplates<ILogger> theLogger;
    theLogger.set(std::move(spLogger));

    std::unique_ptr<IHardwareController> spHwController = std::make_unique<SmarcHardwareController>();
    ServiceLocatorWithTemplates<IHardwareController> theHwController;
    theHwController.set(std::move(spHwController));

    theLogger.get().log("hello");

    theHwController.get().setLed("led1", true);


    somefunction();
}
