#ifndef SERVICELOCATOR_H
#define SERVICELOCATOR_H

#include <QString>
#include <QDebug>


void newfunction() {
   //OK
}

class ILogger {
public:
    virtual void log(QString logmsg) = 0;
};

class ConsoleLogger: public ILogger {
public:
    virtual void log(QString logmsg);
};

class IHardwareController {
public:
    virtual void setLed(QString ledId, bool on) = 0;
};

class SmarcHardwareController: public IHardwareController {
public:
    virtual void setLed(QString ledId, bool on);
};


template<typename T>
struct ServiceLocatorWithTemplates
{
    T& get() { return *m_ptr;}
    void set(std::unique_ptr<T> && ptr) {m_ptr = std::move(ptr);}
private:
    std::unique_ptr<T> m_ptr;
};


#endif // SERVICELOCATOR_H
